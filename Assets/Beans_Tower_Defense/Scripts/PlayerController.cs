﻿using UnityEngine;


// Include the namespace required to use Unity UI
using UnityEngine.UI;

using System.Collections;

public class PlayerController : MonoBehaviour
{

    
    public float speed;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    
    
    private Rigidbody rb;
    private int count;
    bool isFiring = false;
    public float fireRate = .75f;
    float lastShot = 50;
    

    // At the start of the game..
    void Start()
    {
        // Assign the Rigidbody component to our private rb variable
        rb = GetComponent<Rigidbody>();
        
    }



    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Music3"))                   //if player hits the Music3 zone
        {
            fireRate = .35f;
        }

        if (other.gameObject.CompareTag("Music2"))                   //if player hits the Music2 zone
        {
            
            bulletPrefab.transform.localScale = new Vector3(.5f, .5f, .5f);

        }

        if (other.gameObject.CompareTag("Music1"))                   //if player hits the Music1 zone
        {

            bulletPrefab.GetComponent<ParticleSystem>().playOnAwake = true;

        }


    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Music3"))                   //if player exits the Music3 zone
        {
            fireRate = .75f;
        }

        if (other.gameObject.CompareTag("Music2"))                   //if player exits the Music2 zone
        {

            bulletPrefab.transform.localScale = new Vector3(.2f, .2f, .2f);

        }

        if (other.gameObject.CompareTag("Music1"))                   //if player exits the Music1 zone
        {

            bulletPrefab.GetComponent<ParticleSystem>().playOnAwake = false;

        }

    }

    void Update()
    {

        lastShot += Time.deltaTime;

        if (Input.GetButtonDown("Fire1"))
        {
            isFiring = true;
        }

        if (Input.GetButtonUp("Fire1"))
        {
            isFiring = false;
        }

        if (isFiring&&lastShot>fireRate)
        {
            Fire();
            lastShot = 0;
        }

    }



    void Fire()
    {
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            bulletPrefab,
            bulletSpawn.position,
            bulletSpawn.rotation);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;

        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
    }

    // Each physics step..
    void FixedUpdate()
    {
        // Set some local float variables equal to the value of our Horizontal and Vertical Inputs
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        // Create a Vector3 variable, and assign X and Z to feature our horizontal and vertical float variables above
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * speed, ForceMode.Force);

        

    }

    
}
